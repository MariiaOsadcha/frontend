module.exports = {
   sumNumber,
   primeNumber,
   findRoot,
   fact,
   result,
   reWrite
}
//1
function sumNumber() {
    var sum = 0;
    var quantity = (99-1)/2;

    for (var i = 1; i <= 99; i++) {
        if(i % 2 === 0) {
            sum += i;
            quantity++;
        }
    };
    return sum;
    return quantity;
}

//2

function primeNumber(number){
    if (typeof number !== 'number') {
        return 'Error';
    }
    for (var i = 2; i < number/2; i++) {
        if (number % i === 0) {
            return 'Compound number';
        }
    }
    return 'Prime number';
}

//3 

function findRoot(number){
    if (typeof number !== 'number') {
        return 'Ошибка';
    }
    var result = 1;

    for (var i = 1; i**2 <= number; i++) {
        result = i;
    }
    return result;
}


    //4
    
    function fact(number) {
        if (typeof number !== 'number' || number < 0) {
        return 'Ошибка';
        }
        var factorial = 1;
        for (var i = 2; i <= number; i++) {
        factorial *= i;	
        }
        return factorial;
        }


//5

function result(a){
    var f=a;
    var c=0;
    var result=0;
    while(f>0){
        c=f%10;
        result+=c;
        f=(f-c)/10;
    }
    return result;
}

//6

function reWrite(a){
    var f=a;
    var c=0;
    var result=0;
    while(f>0){
        c=f%10;
        result+=c;
        f=(f-c)/10;
        result*=10;
    }
    return result / 10;

}




