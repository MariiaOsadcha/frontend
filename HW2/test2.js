var assert = require('assert');
var object = require('./hw2');
//Task 1
describe("sumNumber", function() {
    it("Find the sum of even numbers and their number in the range from 1 to 99", function() {
        assert.deepEqual(object.sumNumber(), 2450, 49);
    });
});

//Task 2
describe('primeNumber',function() {
    it('Check if a prime number',function() {
        assert.equal (object.primeNumber(19),'Prime number');
        assert.equal (object.primeNumber(5),'Prime number');
        assert.equal (object.primeNumber(45),'Compound number');
        assert.equal (object.primeNumber(50),'Compound number' );
})
});

// Task 3
 describe('findRoot',function() {
    it('Find the root of a natural number up to an integer',function() {
        assert.equal(object.findRoot(9),3);
        assert.equal(object.findRoot(16),4);
        assert.equal(object.findRoot(81),9);
    })
});

//Task 4
describe ('fact',function() {
    it('Calculate the factorial of a number', function() {
        assert.equal(object.fact(3),6);
        assert.equal(object.fact(7),5040);
        assert.equal(object.fact(10),3628800);
    })
});

//Task 5
describe ('result',function() {
    it('Calculate the sum of the digits of a given number', function() {
        assert.equal(object.result(123),6);
        assert.equal(object.result(555),15);
    })
});

//Task 6
describe ('reWrite',function() {
    it('reWrite,like a mirror', function() {
        assert.equal(object.reWrite(123),321);
        assert.equal(object.reWrite(345),543);
    })
});