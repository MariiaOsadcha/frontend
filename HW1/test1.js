var assert = require('assert');
var object = require('./hw1')
//Task 1
describe ('result',function() {
    it ('If  is even, a*b, othervise a+b', function() {
        assert.equal(object.result(4,5),20);
        assert.equal(object.result(10,10),100);
    })
});

//Task 2
describe ('tochka', function() {
    it ('Determine which quarter the point with coordinates (x, y) belongs to',function () {
        assert.equal(object.tochka(3,4),"1 четверть");
        assert.equal(object.tochka(5,10),"1 четверть");
        assert.equal(object.tochka(-3,4),"2 четверть");
        assert.equal(object.tochka(-5,9),"2 четверть");
        assert.equal(object.tochka(-7,-3),"3 четверть");
        assert.equal(object.tochka(-5,-4),"3 четверть");
        assert.equal(object.tochka(6,-7),"4 четверть");
        assert.equal(object.tochka(5,-9),"4 четверть");
        assert.equal(object.tochka(0,0),'Точка находится в начале координат');
        assert.equal(object.tochka(0,4),'Точка находится на оси"х"');
        assert.equal(object.tochka(4,0),'Точка находится на оси"y"')
    })
});

//Task 3
describe('sum', function() {
    it ('Find the sums of only positives from three numbers', function () {
        assert.equal(object.sum(-2,5,6),11);
        assert.equal(object.sum(2,5,6),13);
        assert.equal(object.sum(3,5,1),9);
        assert.equal(object.sum(-2,-3,-5),0);
        assert.equal(object.sum(5,-3,8),13);
    })
});

//Task 4
describe('max', function() {
    it('Calculate the expression max (a * b * c, a + b + c) +3', function() {
        assert.equal(object.max(3,3,3),30);
        assert.equal(object.max(10,3,5),153);
        assert.equal(object.max(10,0,9),22);
        assert.equal(object.max(33,1,1),38);
    })
});

//Task 5
describe('mark', function() {
    it('Write a program for determining student grades according to his rating', function() {
        assert.equal(object.mark(-5),"Такой оценки не сущетвует");
        assert.equal(object.mark(105),"Такой оценки не сущетвует");
        assert.equal(object.mark(15),'F');
        assert.equal(object.mark(19),'F');
        assert.equal(object.mark(25),'E');
        assert.equal(object.mark(38),'E');
        assert.equal(object.mark(55),'D');
        assert.equal(object.mark(47),'D');
        assert.equal(object.mark(62),'C');
        assert.equal(object.mark(72),'C');
        assert.equal(object.mark(75),'B');
        assert.equal(object.mark(85),'B');
        assert.equal(object.mark(90),'A');
        assert.equal(object.mark(100),'A');
    })
});