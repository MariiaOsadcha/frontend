var canvas = document.getElementById('c1');

function goToJSCourse(user) {
    console.log("Мы пошли на курсы...");
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (user.mark < 4) {
                resolve(user)
            } else {
                reject (`Не выполнял ДЗ... ${user.name}` );
            }
        }, 1);
    })
}

function getCertificate(certificate) {
    console.log("getCertificate", certificate);
    return new Promise((resolve, reject) => {
        if (certificate.mark === 5) {
            resolve({ ReactJS: "advanced"});
        } else {
            reject("Знаний всё-равно нет...");
        }
    });
}


var user1 = {
    name: "Oleg",
    age: 20,
    mark: 4,
    skills: ["JS", "React", "WordPress"],
};
var user2 = {
    name: "Tina",
    age: 18,
    mark: 5,
    skills: ["JS", "React", "WordPress"],
};
var user3 = {
    name: "Sergei",
    age: 33,
    mark: 3,
    skills: ["JS", "React", "WordPress"],
};
var user4 = {
    name: "Sasha",
    age: 20,
    mark: 4,
    skills: ["JS", "React", "WordPress"],
};
var user5 = {
    name: "Kirill",
    age: 26,
    mark: 54,
    skills: ["JS", "React", "WordPress"],
};

Promise.race([
    goToJSCourse(user1).then(getCertificate).then(getJobOffer),
    goToJSCourse(user2).then(getCertificate).then(getJobOffer),
    goToJSCourse(user3).then(getCertificate).then(getJobOffer),
    goToJSCourse(user4).then(getCertificate).then(getJobOffer),
    goToJSCourse(user5).then(getCertificate).then(getJobOffer),
])
    .then(name => {
        console.log(`${name} WIN!!!`)
    })
    .catch(error => console.error(error));
