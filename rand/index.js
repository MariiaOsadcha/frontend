var exceptionArray = [];
var output = document.getElementById("output");
var inputMin = document.getElementById("inputMin");
var inputMax = document.getElementById("inputMax");

function getElement() {
  var min = Number(inputMin.value);
  var max = Number(inputMax.value) + 1;

  if(inputMin.value == 0 || inputMax.value == 0 || inputMax.value < inputMin.value) {
  return;
}
  if(exceptionArray.length === (max - min)) {
    output.value = "End";
  }
  var res = generateNum(min,max);
  if(checkNum(exceptionArray, res)) {
    getElement(min,max);
  }
  else {
    exceptionArray.push(res);
    output.value = res;
  }
}
  function generateNum(min,max) {
    return Math.floor(Math.random() * (max - min) + min);

  }
  function checkNum(exceptionArray, res) {
    for(var i = 0; i < exceptionArray.length; i++) {
      if(exceptionArray[i] === res) {
        return true;
      }
    }
    return false;
  }
  function resetAll() {
    document.getElementById("reset");
    exceptionArray = [];
    inputMin.value = "";
    inputMax.value = "";
    output.value = "";
  }
  convert.addEventListener("click", getElement);
  reset.addEventListener("click", resetAll);
