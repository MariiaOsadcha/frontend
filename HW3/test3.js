var assert = require('assert');
var object = require ('./hw3');
//1
describe('getMinElem',function () {
    it('Find the minimum element of an array', function () {
        assert.equal(object.getMinElem([5,6,8,9,1,12]),1);
        assert.equal(object.getMinElem([4,3,2,5,6,7]),2);
    })
});
describe('getMaxElement',function () {
    it('Find the maximum element of an array', function () {
        assert.equal(object.getMaxElem([5,6,8,9,1,12]),12);
        assert.equal(object.getMaxElem([4,3,2,5,6,7]),5);
    })
});
describe('getMinElemIndex', function () {
    it('Find index of minimal element of an array', function () {
        assert.equal(object.getMinElemIndex([5,6,8,9,1,12]),4);
        assert.equal(object.getMinElemIndex([4,3,2,5,6,7]),2);
    })
});
describe('getMaxElemIndex',function () {
    it('Find index of maximum element of array', function () {
        assert.equal(object.getMaxElemIndex([5,6,8,9,1,12]),5);
        assert.equal(object.getMaxElemIndex([4,3,2,5,6,7]),5);

    })
});
describe('getSumOddElem', function () {
    it('Calculate the sum of elements of an array with odd indices', function () {
        assert.equal(object.getSumOddElem([5,6,8,9,1,12]),15);
        assert.equal(object.getSumOddElem([4,3,2,5,6,7]),15);
    })
});
describe('rev', function(){
    it('To do revers', function(){
        assert.equal(object.rev([5,6,8,9,1,12]), [12,1,9,8,6,5],);
        assert.equal(object.rev([4,3,2,5,6,7]), [7,6,5,2,3,4]);
    })
});
describe('findQuantity', function(){
    it('To count number  of odd indices', function(){
        assert.equal(object.findQuantity([5,6,8,9,1,12]),3);
        assert.equal(object.findQuantity([4,3,2,5,6,7]),3);
    })
});
describe('')
