var assert = require('assert');
var object = require('./index');
//1. Если а – четное посчитать а*б, иначе а+б
describe("result", function() {
    it("If a - even count a * b, otherwise a + b", function() {
        assert.equal(object.myFunc(2, 3), 6);
    });
    it("If a - even count a * b, otherwise a + b", function() {
        assert.equal(object.result(3, 3), 6);
    });
    it("If a - even count a * b, otherwise a + b", function() {
        assert.equal(object.result(20, 5), 100);
    });
    it("If a - even count a * b, otherwise a + b", function() {
        assert.equal(object.result(7, 15), 22);
    });
    it("If a - even count a * b, otherwise a + b", function() {
        assert.equal(object.result(12, 3), 36);
    });

});
//2. Определить какой четверти принадлежит точка с координатами (х,у)
describe("coordinates", function() {
    it("Determine which quarter the point with coordinates (x, y) belongs to", function() {
        assert.equal(object.coordinates(2, 3), "Точка находится в I четверти");
    });
    it("Determine which quarter the point with coordinates (x, y) belongs to", function() {
        assert.equal(object.coordinates(-7, -2), "Точка находится в III четверти");
    });
    it("Determine which quarter the point with coordinates (x, y) belongs to", function() {
        assert.equal(object.coordinates(0, 0), "Точка находится на пересечении осей координат");
    });
    it("Determine which quarter the point with coordinates (x, y) belongs to", function() {
        assert.equal(object.coordinates(-3, 3), "Точка находится в II четверти");
    });
    it("Determine which quarter the point with coordinates (x, y) belongs to", function() {
        assert.equal(object.coordinates(2, -3), "Точка находится в IV четверти");
    });
});
//3. Найти суммы только положительных из трех чисел
describe("pol", function() {
    it("Find the sums of only positives from three numbers", function() {
        assert.equal(object.pol(2, 3, 7), 12);
    });
    it("Find the sums of only positives from three numbers", function() {
        assert.equal(object.pol(15, -3, 11), 26);
    });
    it("Find the sums of only positives from three numbers", function() {
        assert.equal(object.pol(-1, 4, -1), 4);
    });
    it("Find the sums of only positives from three numbers", function() {
        assert.equal(object.pol(50, 40, 6), 96);
    });
    it("Find the sums of only positives from three numbers", function() {
        assert.equal(object.pol(-2, -3, -7), 'Нет положительных чисел');
    });
});
//4. Посчитать выражение max(а*б*с, а+б+с)+3
describe("dav", function() {
    it("Calculate the expression max (a * b * s, a + b + s) + 3", function() {
        assert.equal(object.dav(2, 3, 7), 45);
    });
    it("Calculate the expression max (a * b * s, a + b + s) + 3", function() {
        assert.equal(object.dav(4, -3, 11), 15);
    });
    it("Calculate the expression max (a * b * s, a + b + s) + 3", function() {
        assert.equal(object.dav(-1, -13, -10), -21);
    });
    it("Calculate the expression max (a * b * s, a + b + s) + 3", function() {
        assert.equal(object.dav(100, 45, 70), 315003);
    });
    it("Calculate the expression max (a * b * s, a + b + s) + 3", function() {
        assert.equal(object.dav(302, 3, 78), 70671);
    });
});
//5. Написать программу определения оценки студента по его рейтингу, на основе следующих правил
 describe("ratingStudent", function() {
     it("Write a program for determining the student’s grade according to his rating, based on the following rules", function() {
         assert.equal(object.ratingStudent(76), 'B');
     });
     it("Write a program for determining the student’s grade according to his rating, based on the following rules", function() {
         assert.equal(object.ratingStudent(-2), 'Такой бал не существует');
     });
    it("Write a program for determining the student’s grade according to his rating, based on the following rules", function() {
         assert.equal(object.ratingStudent(35), 'E');
     });
     it("Write a program for determining the student’s grade according to his rating, based on the following rules", function() {
         assert.equal(object.ratingStudent(100), 'A');
     });
     it("Write a program for determining the student’s grade according to his rating, based on the following rules", function() {
         assert.equal(object.ratingStudent('value'), 'Ошибка');
     });
 });
