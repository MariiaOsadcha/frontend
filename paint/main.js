let canvas = document.getElementById('canvasBox');
let ctx = canvas.getContext('2d');
let currentColor = 'black';
let currentSize = document.querySelector('.choose-size').value
console.log(currentSize)


document.querySelector('.choose-color').oninput = function(event) {
    currentColor = this.value;
}

document.querySelector('.choose-size').oninput = function(event) {
    currentSize = this.value;
}

document.getElementById('crearButton').onclick = function(event) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

canvas.onmousedown = function(event) {
    canvas.onmousemove = function(event) {
        let posX = event.offsetX;
        let posY = event.offsetY;
        ctx.fillStyle = currentColor;
        ctx.beginPath();
        ctx.arc(posX, posY, currentSize, 0, Math.PI * 2);
        ctx.fill();
    }
    canvas.onmouseup = function() {
        canvas.onmousemove = null;
        let posX = event.offsetX;
        let posY = event.offsetY;
        ctx.fillStyle = currentColor;
        ctx.beginPath();
        ctx.arc(posX, posY, currentSize, 0, Math.PI * 2);
        ctx.fill();
    }
}
