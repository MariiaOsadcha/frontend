const connection = new WebSocket("ws://localhost:8080");
const canv = document.querySelector("#canvas");
let ctx = canv.getContext('2d');
let isMouseDown = false;
let xBlock;
let yBlock;
let pi = Math.PI;
let colorValue = 'red';
let thickValue = '10';
let color = document.querySelectorAll('.color');
let range = document.getElementById('range');

connection.onopen = (event) => {
    console.log("WebSocket is open now.");
};

connection.onclose = (event) => {
    console.log("WebSocket is closed now.");
};

connection.onerror = (event) => {
    console.error("WebSocket error observed:", event);
};

connection.onmessage = (event) => {
    let response = JSON.parse(event.data);

    colorValue = response.color;
    thickValue = response.width;

    if(response.isMouseDown === true){
        draw(response.x, response.y, response.width, response.color);
    }else{
        ctx.beginPath();
    }
};


range.addEventListener('change', function(){
    thickValue = range.value / 10;
});

color.forEach(function(element, index, array){
    element.addEventListener('click', function(){
        color.forEach(function(element, index, array){
            element.style.border = 'none';
        });
        switch(element.classList[0]){
            case 'color-red':
                colorValue = 'red';
                element.style.border = '2px solid black';
                break;
            case 'color-green':
                element.style.border = '2px solid black';
                colorValue = 'green';
                break;
            case 'color-blue':
                element.style.border = '2px solid black';
                colorValue = 'blue';
                break;
        }
    });
});

canv.addEventListener('mousedown', function(){
    isMouseDown = true;
});

canv.addEventListener('mouseup', function(){
    isMouseDown = false;
    ctx.beginPath();
});


canv.addEventListener('mousemove', function(e){
    xBlock = document.querySelector('.xBlock');
    yBlock = document.querySelector('.yBlock');
    xBlock.innerHTML = e.clientX.toString();
    yBlock.innerHTML = e.clientY.toString();


    let body = {
        x: e.clientX,
        y: e.clientY,
        width: thickValue,
        color: colorValue,
        isMouseDown,
    };
    connection.send(JSON.stringify(body));

    if(isMouseDown){
        draw(e.clientX, e.clientY, thickValue, colorValue);
    }
});

const draw = (x, y, lineThink, color) => {
    ctx.strokeStyle = color;
    ctx.lineTo(x, y);
    ctx.lineWidth = lineThink;
    ctx.stroke();

    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.arc(x, y, lineThink/2, 0, pi * 2);
    ctx.fill();

    ctx.beginPath();
    ctx.moveTo(x, y);
};