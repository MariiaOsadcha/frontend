var assert = require('assert');
var object = require('./tasks');


//4.1 Получить строковое название дня недели по номеру дня.

describe("getDayName", function() {

    it("Returns the string name of the day of the week by its number 1", function() {
        var expected = "Sunday";

        var actual = object.getDayName(1);

        assert.equal(actual, expected);
    });

    it("Returns the string name of the day of the week by its number 4", function() {
        var expected = "Wednesday";

        var actual = object.getDayName(4);

        assert.equal(actual, expected);
    });

    it("Returns the string name of the day of the week by its number 7", function() {
        var expected = "Saturday";

        var actual = object.getDayName(7);

        assert.equal(actual, expected);
    });
});

//4.2 Найти расстояние между двумя точками в двумерном декартовом пространстве.

describe("getDistanceBetween", function() {

    it("Finds the distance between two points x (1,2) and y (3,4) in two-dimensional Cartesian space.", function() {
        var expected = 3;

        var actual = object.getDistanceBetween(1,2,3,4);

        assert.equal(actual, expected);
    });

    it("Finds the distance between two points x (4,2) and y (3,1) in two-dimensional Cartesian space.", function() {
        var expected = 1;

        var actual = object.getDistanceBetween(4,2,3,1);

        assert.equal(actual, expected);
    });

    it("Finds the distance between two points x (21,12) and y (23,24) in two-dimensional Cartesian space.", function() {
        var expected = 12;

        var actual = object.getDistanceBetween(21,12,23,24);

        assert.equal(actual, expected);
    });
});

//4.3 Вводим число (0-999), получаем строку с прописью числа.

describe("getStringNumber", function() {

    it("Converts a numeric notation to 1 to lowercase.", function() {
        var expected = "one";

        var actual = object.getStringNumber(1);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 15 to lowercase.", function() {
        var expected = "fifteen";

        var actual = object.getStringNumber(15);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 47 to lowercase.", function() {
        var expected = "forty seven";

        var actual = object.getStringNumber(47);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 40 to lowercase.", function() {
        var expected = "forty ";

        var actual = object.getStringNumber(40);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 440 to lowercase.", function() {
        var expected = "four hundred forty ";

        var actual = object.getStringNumber(440);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 482 to lowercase.", function() {
        var expected = "four hundred eighty two";

        var actual = object.getStringNumber(482);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 400 to lowercase.", function() {
        var expected = "four hundred ";

        var actual = object.getStringNumber(400);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 402 to lowercase.", function() {
        var expected = "four hundred two";

        var actual = object.getStringNumber(402);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 412 to lowercase.", function() {
        var expected = "four hundred twelve";

        var actual = object.getStringNumber(412);

        assert.equal(actual, expected);
    });

});

//4.4 Вводим строку, которая содержит число, написанное прописью (0-999). Получить само число.

describe("getStringEntryToANumber", function() {

    it("It displays the number itself, when you enter a string that contains a number written in words 'one'.", function() {
        var expected = 1;

        var actual = object.getStringEntryToANumber("one");

        assert.equal(actual, expected);
    });

    it("It displays the number itself, when you enter a string that contains a number written in words 'fifteen'.", function() {
        var expected = 15;

        var actual = object.getStringEntryToANumber("fifteen");

        assert.equal(actual, expected);
    });

    it("It displays the number itself, when you enter a string that contains a number written in words 'forty seven'.", function() {
        var expected = 47;

        var actual = object.getStringEntryToANumber("forty seven");

        assert.equal(actual, expected);
    });

    it("It displays the number itself, when you enter a string that contains a number written in words 'four hundred eighty two'.", function() {
        var expected = 482;

        var actual = object.getStringEntryToANumber("four hundred eighty two");

        assert.equal(actual, expected);
    });

});

//4.5 Для задания 3 расширить диапазон до 999 миллиардов.

describe("getStringNumber2", function() {

    it("Converts a numeric notation to 1 to lowercase.", function() {
        var expected = "one";

        var actual = object.getStringNumber2(1);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 15 to lowercase.", function() {
        var expected = "fifteen";

        var actual = object.getStringNumber2(15);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 47 to lowercase.", function() {
        var expected = "forty seven";

        var actual = object.getStringNumber2(47);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 482 to lowercase.", function() {
        var expected = "four hundred eighty two";

        var actual = object.getStringNumber2(482);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 47 372 to lowercase.", function() {
        var expected = "forty seven thousand three hundred seventy two";

        var actual = object.getStringNumber2(47372);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 172 689 to lowercase.", function() {
        var expected = "one hundred seventy two thousand six hundred eighty nine";

        var actual = object.getStringNumber2(172689);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 7 324 613 to lowercase.", function() {
        var expected = "seven million three hundred twenty four thousand six hundred thirteen";

        var actual = object.getStringNumber2(7324613);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 487 143 192 to lowercase.", function() {
        var expected = "four hundred eighty seven million one hundred forty three thousand one hundred ninety two";

        var actual = object.getStringNumber2(487143192);

        assert.equal(actual, expected);
    });

    it("Converts a numeric notation to 412 103 431 012 to lowercase.", function() {
        var expected = "four hundred twelve billion one hundred three million four hundred thirty one thousand twelve";

        var actual = object.getStringNumber2(412103431012);

        assert.equal(actual, expected);
    });
});
