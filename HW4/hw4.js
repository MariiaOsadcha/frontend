module.exports = {
    getDayName,
    getDistanceBetween,
    getStringNumber,
    getStringNumber2,
    getStringEntryToANumber
};

//4.1 Получить строковое название дня недели по номеру дня.

var array = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];

function getDayName(x) {
    for(var i = 0; i <= array.length; i++) {
        if((x-1) === i) {
            return array[i];
        }
    }
}


//4.2 Найти расстояние между двумя точками в двумерном декартовом пространстве.

function getDistanceBetween(x1,y1,x2,y2) {

    return Math.round(Math.sqrt((x2 - x1)* 2 + (y2 - y1)* 2));

}

//4.3 Вводим число (0-999), получаем строку с прописью числа.

function getStringNumber(number) {
    var single = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];
    var fromTenNineteen =  ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];
    var decimal = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];
    var result = '';
    var value = number;
    if (value / 100 >= 1 && value % 100 === 0) {
        result = `${single[Math.floor(number / 100) - 1]} hundred `;
    } else if (value / 100 >= 1 && value % 100 !== 0) {
        result = `${single[Math.floor(number / 100) - 1]} hundred `;
        value = value % 100;
        if (value >= 20 && value < 100 && value % 10 === 0) {
            result += `${decimal[Math.floor(value / 10) - 2]} `;
        } else if (value >= 20 && value < 100 && value % 10 !== 0){
            result += `${decimal[Math.floor(value / 10) - 2]} `;
            value = value % 10;
            result += `${single[value - 1]}`;
        } else if (value >= 10 && value < 20) {
            result += fromTenNineteen[value % 10];
        } else {
            result += `${single[value - 1]}`;
        }
    } else {
        if (value >= 20 && value < 100 && value % 10 === 0) {
            result = `${decimal[Math.floor(value / 10) - 2]} `;
        } else if (value >= 20 && value < 100 && value % 10 !== 0){
            result = `${decimal[Math.floor(value / 10) - 2]} `;
            value = value % 10;
            result += `${single[value - 1]}`;
        } else if (value >= 10 && value < 20) {
            result = fromTenNineteen[value % 10];
        } else if (value < 10) {
            result = `${single[value - 1]}`;
        }
    }
    return result;
}
//4.4 Вводим строку, которая содержит число, написанное прописью (0-999). Получить само число.

function  getStringEntryToANumber(string) {

    var arrStringNumber = string.split(' ');
    var single = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];
    var fromTenNineteen =  ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];
    var decimal = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];
    var result = 0;

    for (var i = 0; i <= arrStringNumber.length; i++) {

        var stringSingle = single.indexOf(arrStringNumber[i]);
        var stringFromTenNineteen = fromTenNineteen.indexOf(arrStringNumber[i]);
        var stringDecimal = decimal.indexOf(arrStringNumber[i]);

        if (i === 0) {
            if (stringSingle !== -1) {
                result = stringSingle + 1;
            }
            if (stringFromTenNineteen !== -1) {
                result = stringFromTenNineteen + 10;
            }
            if (stringDecimal !== -1) {
                result = (stringDecimal + 2) * 10;
            }
        } else if (i === 1) {
            if (arrStringNumber[i] === 'hundred') {
                result *= 100;
            }
            if (stringSingle !== -1) {
                result += stringSingle + 1;
            }
            if (stringFromTenNineteen !== -1) {
                result += stringFromTenNineteen + 10;
            }
            if (stringDecimal !== -1) {
                result += (stringDecimal + 2) * 10;
            }
        } else if (i === 2) {
            if (stringSingle !== -1) {
                result += stringSingle + 1;
            }
            if (stringFromTenNineteen !== -1) {
                result += stringFromTenNineteen + 10;
            }
            if (stringDecimal !== -1) {
                result += (stringDecimal + 2) * 10;
            }
        } else if (i === 3) {
            if (stringSingle !== -1) {
                result += stringSingle + 1;
            }
            if (stringFromTenNineteen !== -1) {
                result += stringFromTenNineteen + 10;
            }
            if (stringDecimal !== -1) {
                result += (stringDecimal + 2) * 10;
            }
        }
    }
    return  result;
}


//5. Для задания 3 расширить диапазон до 999 миллиардов.

var stringNumber = [
    ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"],
    ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"],
    ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]
]

function getStringNumber2(number) {
    var longNumbers = String(number).length;
    if (longNumbers < 3) {
        return getStringTwoElementsNumber(number);
    } else if (longNumbers == 3) {
        return getStringThreeElementsNumber(number);
    } else if (longNumbers > 3 && longNumbers < 7) {
        return getStringNumberUpMillion(number);
    } else if (longNumbers > 6 && longNumbers < 10) {
        return getStringNumberUpBillion(number);
    } else if (longNumbers > 9 && longNumbers < 13) {
        return getLineNumberOverBillion(number);
    } else {
        return "Слишком большое число";
    }
}

function getStringTwoElementsNumber(number){
    if(number > 0 && number < 10){
        return stringNumber[0][number - 1];
    }else if(number >= 10 && number < 20){
        return stringNumber[1][number % 10];
    }else if(number >= 20 && number < 100){
        if(number % 10 === 0){
            return stringNumber[2][number/10 - 2];
        }else{
            return stringNumber[2][(number - number % 10) / 10 - 2] + " " + stringNumber[0][number % 10 - 1];
        }
    }
}

function getStringThreeElementsNumber(number) {
    if (number % 100 === 0) {
        return stringNumber[0][number / 100 - 1] + " hundred";
    } else {
        return stringNumber[0][(number - number % 100) / 100 - 1] + " hundred" + " " + getStringTwoElementsNumber(number % 100);
    }
}

function getStringNumberUpMillion(number) {
    if (number % 1000 === 0) {
        return getStringNumber2(number / 1000) + " thousand";
    } else if (number % 1000 >= 100){
        return getStringNumber2((number - number % 1000) / 1000) + " thousand" + " " + getStringThreeElementsNumber(number % 1000);
    } else {
        return getStringNumber2((number - number % 1000) / 1000) + " thousand" + " " + getStringTwoElementsNumber(number % 1000);
    }
}


function getStringNumberUpBillion(number) {
    if (number % 1000000 === 0) {
        return getStringNumber2(number / 1000000) + " million";
    } else {
        return getStringNumber2((number - number % 1000000) / 1000000) + " million" + " " + getStringNumberUpMillion(number - (number - number % 1000000));
    }
}

function getLineNumberOverBillion(number) {
    if (number % 1000000000 === 0) {
        return getStringNumber2(number / 1000000000) + " billion";
    } else {
        return getStringNumber2((number - number % 1000000000) / 1000000000) + " billion" + " " + getStringNumberUpBillion(number - (number - number % 1000000000));
    }
}